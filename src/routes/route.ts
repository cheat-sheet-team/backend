async function routes (fastify, options) {
  //const collection = fastify.mongo.db.collection('users')

  fastify.get('/', async () => {
    return {hello: "hello"}
  })

  fastify.get('/2', async () => {
    return {hello: 'world2'}
  })

  // fastify.get('/users', async () => {
  //   const result = await collection.find().toArray()
  //   if (result === null) {
  //     throw new Error('no document founded')
  //   }
  //   return result
  // })
  //
  // fastify.get('/user/name/:name', async (request) => {
  //   const result = await collection.findOne({name: request.params.name})
  //   if (result === null) {
  //     throw new Error('no document founded')
  //   }
  //   return result
  // })
}

module.exports = routes