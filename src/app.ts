require('dotenv').config()
const fastify = require('fastify')({
  logger: true
  //,pluginTimeout: 50000
})

// get needed value from env
const { IP_ADDR, PORT } = process.env

// plugins
//fastify.register(require('./database/connector'))
fastify.register(require('./routes/route'))


/**
 * server starting
 * @returns {Promise<void>}
 */
const start = async () => {
  try {
    await fastify.listen(PORT, IP_ADDR)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start().then()