FROM node:14.17.1-alpine

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

EXPOSE 3000

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY . .

CMD [ "npm", "run", "start:prod" ]